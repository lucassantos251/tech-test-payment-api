using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class VendaController : ControllerBase
  {
    private readonly CaixaContext _context;

    public VendaController(CaixaContext context)
    {
      _context = context;
    }

    [HttpPost]
    public IActionResult Criar(Venda venda)
    {
      if (venda.DataDaVenda == DateTime.MinValue)
        return BadRequest(new { Erro = "A data da venda não pode ser vazia." });

      if (venda.Id != 0)
      {
        venda.Id = 0;
      }

      if (venda.Status != Models.EnumStatusVenda.AguardandoPagamento)
      {
        venda.Status = Models.EnumStatusVenda.AguardandoPagamento;
      }

      if (ModelState.IsValid)
      {
        _context.Add(venda);
        _context.SaveChanges();
        return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
      }

      return BadRequest(new { Erro = "O objeto não é válido." });
    }

    [HttpGet("{id}")]
    public IActionResult ObterPorId(int id)
    {
      var vendaBanco = _context.Caixa.Find(id);

      if (vendaBanco == null)
        return NotFound();

      return Ok(_context.Caixa.Find(id));
    }

    [HttpGet("ObterTodos")]
    public IActionResult ObterTodos()
    {
      var vendaBanco = _context.Caixa.ToList();

      return Ok(vendaBanco);
    }

    [HttpPut("{id}")]
    public IActionResult Atualizar(int id, Venda venda)
    {
      var vendaBanco = _context.Caixa.Find(id);

      if (vendaBanco == null)
        return NotFound();

      // if (venda.Status != Models.EnumStatusVenda.Cancelada)
      // {
      //   if (vendaBanco.Status != Models.EnumStatusVenda.Cancelada)
      //   {
      //     vendaBanco.Status = venda.Status;
      //   }
      //   else
      //   {
      //     vendaBanco.Status = Models.EnumStatusVenda.Cancelada;
      //   }
      // }
      // else
      // {
      //   vendaBanco.Status = Models.EnumStatusVenda.Cancelada;
      // }

      switch (venda.Status)
      {

        case Models.EnumStatusVenda.PagamentoAprovado:
          if (vendaBanco.Status == Models.EnumStatusVenda.AguardandoPagamento)
            vendaBanco.Status = Models.EnumStatusVenda.PagamentoAprovado;
          break;

        case Models.EnumStatusVenda.EnviadoParaTransportadora:
          if (vendaBanco.Status == Models.EnumStatusVenda.PagamentoAprovado)
            vendaBanco.Status = Models.EnumStatusVenda.EnviadoParaTransportadora;
          break;

        case Models.EnumStatusVenda.Entregue:
          if (vendaBanco.Status == Models.EnumStatusVenda.EnviadoParaTransportadora)
            vendaBanco.Status = Models.EnumStatusVenda.Entregue;
          break;

        case Models.EnumStatusVenda.Cancelada:
          vendaBanco.Status = Models.EnumStatusVenda.Cancelada;
          break;
      }

      _context.Caixa.Update(vendaBanco);
      _context.SaveChanges();

      return Ok(_context.Caixa.Find(id));
    }
  }
}