using tech_test_payment_api.Models;

namespace tech_test_payment_api.Entities
{
  public class Venda
  {
    public int Id { get; set; }
    public DateTime DataDaVenda { get; set; }
    public Vendedor Vendedor { get; set; }
    public List<Produto> ItensVendidos { get; set; }
    public EnumStatusVenda Status { get; set; }
  }
}